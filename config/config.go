package config

import "github.com/spf13/viper"

type Config struct {
	Release      bool
	AppName      string
	HttpPort     string
	AuthHost     string
	AuthPort     string
	Service1Host string
	Service1Port string
	LogDir       string
	LogFile      string
}

// InitConfig - load config from config.yml
func InitConfig() Config {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	conf := Config{
		Release:      viper.GetBool("release"),
		AppName:      viper.GetString("name"),
		HttpPort:     viper.GetString("http_port"),
		AuthHost:     viper.GetString("auth.host"),
		AuthPort:     viper.GetString("auth.port"),
		Service1Host: viper.GetString("service1.host"),
		Service1Port: viper.GetString("service1.port"),
		LogDir:       viper.GetString("log.dir"),
		LogFile:      viper.GetString("log.file"),
	}

	return conf
}
