package http

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/some_prodject_on_microservices/gateway/app"
)

type AuthMiddleware struct {
	usecase app.Usecase
}

// NewAuthMiddleware ...
func NewAuthMiddleware(usecase app.Usecase) gin.HandlerFunc {
	return (&AuthMiddleware{
		usecase: usecase,
	}).Handle
}

// Handle ...
func (m *AuthMiddleware) Handle(c *gin.Context) {
	authHeader := c.GetHeader("Authorization")
	if authHeader == "" {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	headerParts := strings.Split(authHeader, " ")
	if len(headerParts) != 2 {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if headerParts[0] != "Bearer" {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	user, err := m.usecase.ParseToken(c.Request.Context(), headerParts[1])
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	c.Set(CtxUserKey, user)
}
