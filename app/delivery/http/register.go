package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/some_prodject_on_microservices/gateway/app"
)

// RegisterHTTPEndpoints ...
func RegisterAuthHTTPEndpoints(router *gin.RouterGroup, uc app.Usecase) {
	h := NewHandler(uc)

	authEndpoints := router.Group("/auth")
	{
		authEndpoints.POST("/signup", h.SignUp)
		authEndpoints.POST("/signin", h.SignIn)
	}
}

func RegisterTicketHTTPEndpoints(router *gin.RouterGroup, uc app.Usecase, middlewares ...gin.HandlerFunc) {
	h := NewHandler(uc)

	ticketEndpoints := router.Group("/ticket")
	ticketEndpoints.Use(middlewares...)
	{
		ticketEndpoints.POST("/new", h.NewTicket)
	}
}
