package http

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	authpb "gitlab.com/some_prodject_on_microservices/api/auth"
	service1pb "gitlab.com/some_prodject_on_microservices/api/service1"

	"gitlab.com/some_prodject_on_microservices/gateway/app"
)

type Handler struct {
	uc app.Usecase
}

func NewHandler(uc app.Usecase) *Handler {
	return &Handler{
		uc: uc,
	}
}

type inpUser struct {
	Email string `json:"email"`
	Pass  string `json:"pass"`
}

func (h *Handler) toAuthPbUser(user inpUser) *authpb.User {
	return &authpb.User{
		Email: user.Email,
		Pass:  user.Pass,
	}
}

func (h *Handler) SignUp(c *gin.Context) {
	var user inpUser

	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
		return
	}

	token, err := h.uc.SignUp(context.Background(), h.toAuthPbUser(user))
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"token": token})
}

func (h *Handler) SignIn(c *gin.Context) {
	var user inpUser

	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
		return
	}

	token, err := h.uc.SignIn(context.Background(), h.toAuthPbUser(user))
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"token": token})
}

func (h *Handler) NewTicket(c *gin.Context) {
	user, _ := c.Get(CtxUserKey)

	id, err := h.uc.NewTicket(context.Background(), &service1pb.Ticket{
		User: &service1pb.User{
			ID:    user.(*authpb.User).ID,
			Email: user.(*authpb.User).Email,
		},
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{"id": id})
}
