package usecase

import (
	"context"
	"fmt"

	authpb "gitlab.com/some_prodject_on_microservices/api/auth"
	service1pb "gitlab.com/some_prodject_on_microservices/api/service1"
	"gitlab.com/some_prodject_on_microservices/gateway/app"
	"google.golang.org/grpc"
)

type usecase struct {
	AuthClient   authpb.AuthClient
	TicketClient service1pb.Service1Client
}

func NewUsecase(authServer, service1Server string) app.Usecase {
	authConn, err := grpc.Dial(authServer, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	service1Conn, err := grpc.Dial(service1Server, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	return &usecase{
		AuthClient:   authpb.NewAuthClient(authConn),
		TicketClient: service1pb.NewService1Client(service1Conn),
	}
}

func (u *usecase) SignUp(ctx context.Context, user *authpb.User) (string, error) {
	res, err := u.AuthClient.SignUp(ctx, user)
	if err != nil {
		return "", err
	}

	return res.Token, nil
}

func (u *usecase) SignIn(ctx context.Context, user *authpb.User) (string, error) {
	res, err := u.AuthClient.SignIn(ctx, user)
	if err != nil {
		return "", err
	}

	return res.Token, nil
}

func (u *usecase) ParseToken(ctx context.Context, token string) (*authpb.User, error) {
	res, err := u.AuthClient.ParseToken(ctx, &authpb.InputToken{Token: token})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return res, nil
}

func (u *usecase) NewTicket(ctx context.Context, ticket *service1pb.Ticket) (*service1pb.TicketID, error) {
	res, err := u.TicketClient.NewTicket(ctx, ticket)
	if err != nil {
		return nil, err
	}

	return res, nil
}
