package app

import (
	"context"

	authpb "gitlab.com/some_prodject_on_microservices/api/auth"
	service1pb "gitlab.com/some_prodject_on_microservices/api/service1"
)

// Usecase ...
type Usecase interface {
	SignUp(ctx context.Context, user *authpb.User) (string, error)
	SignIn(ctx context.Context, user *authpb.User) (string, error)
	ParseToken(ctx context.Context, token string) (*authpb.User, error)

	NewTicket(ctx context.Context, ticket *service1pb.Ticket) (*service1pb.TicketID, error)
}
