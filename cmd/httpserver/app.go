package httpserver

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/some_prodject_on_microservices/gateway/app"
	apphttp "gitlab.com/some_prodject_on_microservices/gateway/app/delivery/http"
	appusecase "gitlab.com/some_prodject_on_microservices/gateway/app/usecase"
	"gitlab.com/some_prodject_on_microservices/gateway/config"
)

// App ...
type App struct {
	conf       config.Config
	appUC      app.Usecase
	httpServer *http.Server
}

// NewApp ...
func NewApp(conf config.Config) *App {
	uc := appusecase.NewUsecase(
		fmt.Sprintf("%s:%s", conf.AuthHost, conf.AuthPort),
		fmt.Sprintf("%s:%s", conf.Service1Host, conf.Service1Port),
	)

	return &App{
		conf:  conf,
		appUC: uc,
	}
}

// Run run application
func (a *App) Run() {
	router := gin.New()
	if viper.GetBool("app.release") {
		gin.SetMode(gin.ReleaseMode)
	} else {
		router.Use(gin.Logger())
	}

	apiRouter := router.Group("/api")

	authMiddlware := apphttp.NewAuthMiddleware(a.appUC)
	apphttp.RegisterAuthHTTPEndpoints(apiRouter, a.appUC)

	apphttp.RegisterTicketHTTPEndpoints(apiRouter, a.appUC, authMiddlware)

	a.httpServer = &http.Server{
		Addr:           ":" + a.conf.HttpPort,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	var l net.Listener
	var err error
	l, err = net.Listen("tcp", a.httpServer.Addr)
	if err != nil {
		panic(err)
	}

	if err := a.httpServer.Serve(l); err != nil {
		log.Fatalf("Failed to listen and serve: %+v", err)
	}
}

func (a *App) Stop() error {
	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	return a.httpServer.Shutdown(ctx)
}
