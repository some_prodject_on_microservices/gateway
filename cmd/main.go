package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/some_prodject_on_microservices/gateway/cmd/httpserver"
	"gitlab.com/some_prodject_on_microservices/gateway/config"
	"gitlab.com/some_prodject_on_microservices/gateway/pkg/logger"
)

func main() {
	conf := config.InitConfig()

	logger.InitLogger(conf)

	apphttp := httpserver.NewApp(conf)
	go apphttp.Run()

	fmt.Println(
		fmt.Sprintf(
			"Service %s is running",
			conf.AppName,
		),
	)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	apphttp.Stop()

	fmt.Println(
		fmt.Sprintf(
			"Service %s is stopped",
			conf.AppName,
		),
	)
}
